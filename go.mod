module github.com/vs49688/mailpump

go 1.16

require (
	github.com/emersion/go-imap v1.2.0
	github.com/emersion/go-message v0.15.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
)

require github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
